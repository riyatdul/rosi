part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController textUsername = TextEditingController();
  TextEditingController textEmail = TextEditingController();
  TextEditingController textPassword = TextEditingController();
  TextEditingController textConfPassword = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
        elevation: 0.0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        iconTheme: IconThemeData(color: Colors.grey),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Register",
              style: TextStyle(
                  color: Color.fromARGB(255, 212, 30, 17),
                  fontWeight: FontWeight.bold,
                  fontSize: 38),
            ),
            Text(
              "Make an account",
              style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w900,
                  fontSize: 18),
            ),
            SizedBox(
              height: 16,
            ),
            Form(
                key: formKey,
                child: Column(
                  children: [
                    FormInputText(
                      controller: textEmail,
                      label: "Username",
                      hint: "ex. denisdoe",
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Please a Enter';
                        }
                        return null;
                      },
                    ),
                    FormInputText(
                      controller: textPassword,
                      label: "Password",
                      hint: "Enter your password",
                      obscure: true,
                      suffixIcon: Icon(
                        Icons.remove_red_eye_outlined,
                        color: Colors.grey,
                      ),
                      validator: (String? value) {
                        if (value!.length < 4) {
                          return 'Min 4 character password';
                        }
                        return null;
                      },
                    ),
                    FormInputText(
                      controller: textConfPassword,
                      label: "Confirm Password",
                      hint: "Enter your password",
                      obscure: true,
                      suffixIcon: Icon(
                        Icons.remove_red_eye_outlined,
                        color: Colors.grey,
                      ),
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Please re-enter password';
                        }
                        if (textPassword.text != textConfPassword.text) {
                          return "Password does not match";
                        }
                        return null;
                      },
                    ),
                    FormInputText(
                      label: "Number Phone",
                      hint: "eg. 08232418xxx",
                      textInputType: TextInputType.number,
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Please enter phone number';
                        }
                        return null;
                      },
                    ),
                    FormInputText(
                      label: "Email",
                      hint: "eg. denisdoe@roketin.com",
                      textInputType: TextInputType.emailAddress,
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Please enter Email';
                        }
                        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                            .hasMatch(value)) {
                          return 'Please a valid Email';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Forgot Password",
                        style: TextStyle(
                            color: Colors.black38, fontWeight: FontWeight.w900),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      width: double.infinity,
                      child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                                Color.fromARGB(255, 212, 30, 17)),
                            padding: MaterialStateProperty.resolveWith<
                                EdgeInsetsGeometry>(
                              (Set<MaterialState> states) {
                                return EdgeInsets.symmetric(vertical: 16);
                              },
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (formKey.currentState!.validate() == true) {
                              Get.to(IlustrationPage());
                            } else {
                              print("gaga;");
                            }
                          },
                          child: Text(
                            "Register",
                            style: TextStyle(
                                fontWeight: FontWeight.w900, fontSize: 16),
                          )),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(
                          text: 'Already have an account?',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Get.back();
                            },
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black54,
                              fontFamily: "Poppins",
                              fontSize: 14),
                          children: <TextSpan>[
                            TextSpan(
                              text: ' Back to Login',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontFamily: "Poppins",
                                  color: Color.fromARGB(255, 212, 30, 17),
                                  fontSize: 14),
                            )
                          ]),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
