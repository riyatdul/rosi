part of 'pages.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController textUsername = TextEditingController();
  TextEditingController textPassword = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2,
                height: 150,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/logo-rise.png"))),
              ),
              Form(
                  key: formKey,
                  child: Column(
                    children: [
                      FormInputText(
                        controller: textUsername,
                        label: "Username",
                        hint: "ex. denisdoe",
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return 'Please a Enter';
                          }

                          return null;
                        },
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      FormInputText(
                        controller: textPassword,
                        obscure: true,
                        suffixIcon: Icon(
                          Icons.remove_red_eye_outlined,
                          color: Colors.grey,
                        ),
                        label: "Password",
                        hint: "Enter your password",
                        validator: (String? value) {
                          if (value!.length < 4) {
                            return 'Min 4 character password';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Forgot Password",
                          style: TextStyle(
                              color: Colors.black38,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Color.fromARGB(255, 212, 30, 17)),
                              padding: MaterialStateProperty.resolveWith<
                                  EdgeInsetsGeometry>(
                                (Set<MaterialState> states) {
                                  return EdgeInsets.symmetric(vertical: 16);
                                },
                              ),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                              ),
                            ),
                            onPressed: () {
                              if (formKey.currentState!.validate() == true) {
                                print("sukses");
                              } else {
                                print("gagal");
                              }
                            },
                            child: Text("Login")),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RichText(
                        text: TextSpan(
                            text: 'Want to user Roketin Attendance?',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black54,
                                fontFamily: "Poppins",
                                fontSize: 14),
                            children: <TextSpan>[
                              TextSpan(
                                text: ' Get it Now',
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Get.to(SignUpPage());
                                  },
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontFamily: "Poppins",
                                    color: Color.fromARGB(255, 212, 30, 17),
                                    fontSize: 14),
                              )
                            ]),
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
