part of 'pages.dart';

class IlustrationPage extends StatefulWidget {
  const IlustrationPage({Key? key}) : super(key: key);

  @override
  State<IlustrationPage> createState() => _IlustrationPageState();
}

class _IlustrationPageState extends State<IlustrationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2,
                height: 150,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/checklist-rise.png"))),
              ),
              Text(
                "Register Success",
                style: TextStyle(
                    color: Color.fromARGB(255, 212, 30, 17),
                    fontWeight: FontWeight.bold,
                    fontSize: 38),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                "You have successsfully registered an account at Roketin Attendance. Please check your email, you need to confirm your account by clicking the link we have sent you",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 14,
                    color: Colors.black54),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 30),
        child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all(Color.fromARGB(255, 212, 30, 17)),
              padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
                (Set<MaterialState> states) {
                  return EdgeInsets.symmetric(vertical: 16);
                },
              ),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
              ),
            ),
            onPressed: () {},
            child: Text(
              "Okay",
              style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16),
            )),
      ),
    );
  }
}
