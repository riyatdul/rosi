import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../widgets/widgets.dart';

part 'sign_up_page.dart';
part 'sign_in_page.dart';
part 'ilustrator_page.dart';
