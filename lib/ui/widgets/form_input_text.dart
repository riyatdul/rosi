part of 'widgets.dart';

class FormInputText extends StatelessWidget {
  final String label;
  final String hint;
  final TextEditingController? controller;
  final int lines;
  final Function()? press;
  final String? Function(String?)? validator;
  final Widget? suffixIcon;
  final TextInputType? textInputType;
  final bool obscure;

  const FormInputText(
      {Key? key,
      required this.label,
      required this.hint,
      this.lines = 1,
      this.controller,
      this.press,
      this.suffixIcon,
      this.validator,
      this.obscure = false,
      this.textInputType = TextInputType.text})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$label",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black87,
                  fontWeight: FontWeight.w900)),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            controller: controller,
            maxLines: lines,
            validator: validator,
            keyboardType: textInputType,
            obscureText: obscure,
            decoration: InputDecoration(
                hintText: "$hint",
                hintStyle: TextStyle(color: Colors.black54.withOpacity(0.4)),
                // fillColor: ,
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(
                      color: Color.fromARGB(141, 158, 158, 158), width: 4.0),
                  borderRadius: BorderRadius.circular(2.0),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(
                      color: Color.fromARGB(141, 158, 158, 158), width: 4.0),
                  borderRadius: BorderRadius.circular(2.0),
                ),
                suffixIcon: suffixIcon),
          ),
          SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}
